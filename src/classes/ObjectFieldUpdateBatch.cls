global class ObjectFieldUpdateBatch implements Database.Batchable<sObject>, Database.Stateful {

	//String query=System.Label.Batch_Query;
	global String query;
	public String actionField;
	public String actionValue;

	global ObjectFieldUpdateBatch() {

	}

	global Database.QueryLocator start(Database.BatchableContext BC) {

		for (Batch_Query__c bq : Batch_Query__c.getAll().values()) {
			if (bq.active__c == true) {
				query = 'select ' + bq.Action_Field__c + ' From ' + bq.SF_Object__c + ' ' + bq.Query__c;
				actionField = bq.Action_Field__c;
				actionValue = bq.Action_Value__c;
			}
		}

		if(Test.isRunningTest())
		    query = query + ' Limit 200';
		system.debug('query ' + query);
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<SObject> scope) {
		List<SObject> listUpdate = new List<SObject>();
		
		for (SObject a : scope) {
			a.put(actionField, actionValue);	
			listUpdate.add(a);
		}
		if (listUpdate.size() > 0) {
			try {
				database.update(listUpdate, false);
			} catch (Exception e) {
				system.debug(e.getMessage());
			}
		}

	}
	global void finish(Database.BatchableContext BC) {

	}

}